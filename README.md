DOLEZITE DATUMY:

**do 1.2.  :**

1. prezentace prakticke
2. Teoreticka cast kolko toho zvladnem mat. Musia vsak byt uz ucelene a kompletne.

**do 3. 3. 2019 Přihlášení k SZZ zápisem předmětů SOBHA a SZMGR**

**do 18. 3. 2019 Potvrzení oficiálního zadání diplomové práce v ISu studentem a vedoucím**

**do 5. 4. 2019 Schválení oficiálního zadání garanty oborů**

**od 23. 4. 2019 Tisk oficiálního zadání diplomové práce**

**do 1.5.2019 Odovzdanie diplomky - pre skutocny datum pozriet spodok tohto suboru**

Vedení:

RNDr. Tomáš Rebok, Ph.D., učo 39685 (vedoucí)   

Mgr. Slávek Licehammer, učo 255920 (oponent)   

Garanční pracoviště tématu: Katedra počítačových systémů a komunikací

Oficiální zadání:

Tradiční přístupy správy síťových firewallů využívají specifikace filtrovacích pravidel na základě statických a správcům firewallu oznamovaných síťových adres či síťových rozsahů, což výrazně omezuje flexibilitu dynamických prostředí (například univerzitního typu). Cílem této práce je prozkoumat a vyhodnotit možnosti správy firewallových pravidel založených na identitách uživatelů a řízených objektů, a to jak v prostředí tradičních sítí, tak i v případě moderních přístupů založených na principech softwarově-definovaných sítí. Pro návrh a prototypovou realizaci takového systému řizení firewallu, umožňujícího i delegaci řízení na lokální správce skupin, student využije existující systém pro správu identit osob Masarykovy univerzity (systém Perun). Student navrhne rozšíření systému Perun umožňující udržování síťových adres či síťových rozsahů uživatelů, a tento vhodně propojí s vytvořeným systémem pro správu firewallových pravidel, umožňujícím řízení alespoň dvou různých firewallových systémů. V textové části práce se student bude věnovat jak vlastní analýze možností řízení firewallových pravidel na základě identit uživatelů, tak i důkladnému popisu architektury navrhovaného systému, nejdůležitějším implementačním detailům a jeho evaluaci.

Osnova:
1. Introduction
2. Firewall
   * Typology
   * Nasadzovanie FW v sieti
   * NAT a VPN
   * Deploying - globalne principy a niektore odlisnosti vo firewalloch
3. Identity management systems
   * Identity and access management - obecne plus niekolko inych systemov
   * Perun
     * Pojmy entit vo VO / mimo VO
     * propagacia konfiguracie
4. Identity-based firewall management (security systems)
   * Cisco a Juniper Identitty
   * poziadavky na system
5. NAvrh architektura systemu a implementace
   * Ansible
   * Zmeny v perunovi
   * Priebeh zivota volania kodu (stavovy diagram?)
     * problemy s niektorymi funkciami, ich riesenia
6. Deployment of the system into next generation of networks
   * SDN
   * OpenFlow - OvS
   * Netconf
   * OpenStack
7. Evaluation and Local testing
   * Popis testovacieho prostredia:
     * popis topologii
     * popis konfiguracii (perunich konf.)
   * Vyhodnotenie testov
8. Summary


> Kapitola o FW -
> v uvode predstavene rozne typy firewallov, rozne typy
>
>
> obecna konfiguracia pravidiel, podla coho sa daju stavat
>
> Identity manag - m
>
> Analyza poziadavkov 4. kapitola uvod co to bude robit ten tool a analyza poziadavkov
> jedna z poziadavok je nezavislost na vednrooch, oni sa lisia a preto  
> Navrh systemu - Ako to cele do seba zapada - vykonavanie
>
> identity manag. existuje kopec systemov - obecne, par spomenut ich rozne pristupy, pripade ze su to komercne nastroje.


Rozvrh prace:

| tyden   | tyden od |  do      | ukol - prakticka cast                                               | ukol - text                      |
| ------- | -------- | -------- | ------------------------------------------------------------------- | -------------------------------- |
| Week 0  | 3.12.    | 9.12.    |  EOS - end of semester                                              |                                                              |
| Week 1  | 10.12.   | 16.12.   | juniper a cisco netobjecty zjednotit v kode                         |   Kapitola 1, kontrola 1                                     |
| Week 2  | 17.12.   | 23.12.   | SR: dovolenka                                                       |                                                              |  
| Week 3  | 24.12.   | 30.12.   | juniper a cisco netobjecty zjednotit v kode                         |                                                              |
| Week 4  | 31.12.   | 6.1.     | juniper a cisco netobjecty zjednotit v kode                         |                                                              |
| Week 5  | 7.1.     | 13.1.    |                                                                     |    Kapitola 2, kontrola                                      |
| Week 6  | 14.1.    | 20.1.    |                                                                     |                                                              |
| Week 7  | 21.1.    | 27.1.    | lokalne test. - dotiahnut impl. (aby prekladala)                    |    Kapitola 1,2 - kontrola 2                                 |
| Week 8  | 28.1.    | 3.2.     |                           dotiahnut impl. (aby prekladala)          |   Kap 3 - kontrola, + pridat Cisco, Juniper identity based fw|
| Week 9  | 4.2.     | 10.2.    | OOP +?cisco                                                         |                                                              |
| Week 10 | 11.2.    | 17.2.    | Cisco impl.                                                         |   finalizovat po kapitolu 3 vcetne                           |
| Week 11 | 18.2.    | 24.2.    | Impl. vylepseni / schopnosti prekladu cisco <-> junos               |   Kapitola 4                                                 |                             
| Week 12 | 25.2.    | 3.3.     | Impl. vylepseni / schopnosti prekladu cisco <-> junos               |                                                              |
| Week 13 | 4.3.     | 10.3.    | fin. testy - na zaklade uz naprovisnovanych vagrantich boxov        |  Kapitola 6                                                  |
| Week 14 | 11.3.    | 17.3.    | ---                                                                 |                                                              |                             
| Week 15 | 18.3.    | 24.3.    |                                                                     |                                                              |
| Week 16 | 25.3.    | 31.3     |                                                                     |                                                              |
| Week 17 | 1.4.     | 7.4.     |                                                                     |                                                              |
| Week 18 | 8.4.     | 14.4.    |                                                                     |                                                              |
| Week 19 | 15.4.    | 21.4.    |                                                                     |   Gramaticka korektura                                       |                             
| Week 20 | 22.4.    | 28.4.    |                                                                     |   Opravy - proofreading                                      |
| Week 21 | 29.4.    | 5.5.     |                                                                     |   Odovzdanie                                                 |



---
Konzultace 23.1.:
poslat nejaky text ohladom odborneho stylu ako ukazku, ci pisem prvu osobu (pl) alebo pasiv







---
TODOS:

do piatku 7.12. 12:30 - 13:30 gotex

Osnova prvych troch kapitol pridat body ako by to malo byt finalizovane. Vytisknut a sponzamkovat.

Rozlusknut pristup ku objektom.

---
~~11. 10. 2018 - Potvrzení oficiálního zadání diplomové práce v ISu studentem a vedoucím~~

~~27. 10. 2018 - Schválení oficiálního zadání garanty oborů~~

~~12. 12. 2018 do 11.30  - Odevzdání diplomových prací a Prohlášení autora školního dílana studijní oddělení~~

---
osnova:
Delegation of firewall management

1. Introduction
2. Firewall -  
   * Cast o firewalloch z teoretickej stranky - jednoduche stateless FW  
   stateful, nextgen - deep packet inspection a ine,  
   * vyuzitie FW v sieti, rozne pristupy k umiestneniu pred / za / GW  
   pristupy k pravidlam - source based  vs. destination based.  
   * NAT a VPN
   * Deploying - globalne principy a niektore odlisnosti vo firewalloch
3. Perun identity management tool -  
   Popis peruna a jeho casti vyznamnych pre tuto pracu, popis propagacie
4. Firewalling in SDN   
   moznosti FWaaS pre cloudy,  
   moznosti v OpenFlow sietach (stateful/less),
   Openstack a ovs - stafefull FW podpora ?
5. Implementation   
   Cast o implementacii, poradie vykonavania operacii pocas behu  
Ziskanie runcfg -> ziskanie info o zonach -> priradenie zon k ACL -> priradenie pravidiel k ACL -> preklad a umiestnenie novych ACL + agregacia/optimalizacia -> Kontrola nezmenenej runcfg/pristupu -> upload noveho runcfg
6. Local testing  
   Popis testovacej infrastruktury/infrastruktur, testovacich nastrojov (overenie (ne)schopnosti prenosu cez netcat /+scapy/ ).
7. Summary

| tyden | tyden od | tyden do | ukol - prakticka cast   | ukol - text |
| ----- | -------- | -------- | ----------------------- | ------------|
| 1     |    20.8. |    27.8. | ASA - fin. v9.2 zaklad  | priprava osnova + zadanie              |
| 2     | 27.8.    | 2.9.     | ASA - rozsirenia        | kapitola FW (nosna teoreticka kap.)    |
| 3     | 3.9.     | 9.9.     | ASA - rozsirenia / v9.3 | kapitola FW cont.                      |
| 4     | 10.9.    | 16.9.    | JUNOS zaklad            | kapitola Perun                         |
| 5     | 17.9.    | 23.9.    | JUNOS - rozsirenia      | Perun cont.                            |
| 6     | 24.9.    | 30.9.    | + tyzden na sklz +      | SDN kapitola                           |
| 7     | 1.10     | 7.10.    | prepojenie s perunom    | SDN kapitola                           |
| 8     | 8.10.    | 14.10.   | prepojenie s perunom    | kap. o implementacii                   |
| 9     | 15.10.   | 21.10.   | celkove testovanie      | kapitola o testovani                   |
| 10    | 22.10.   | 28.10    | uprava/komentare        | Zaver                                  |
| 11    | 29.10.   | 4.11.    | + tyzden na sklz +      |  + tyzden pre pripad sklzu +           |
| 12    | 05.11.   | 11.11.   | priprava balicka        |  Kontrola textu (formalna)             |
| 13    | 12.11.   | 18.11.   | -tak ako bude odovzdany | text odovzdany na kontrolu (ako celok) |
| 14    | 19.11.   | 26.11.   |    -                    | finalizovany text - oprava chyb        |

**do 20. 5. 2019 do 11.30Odevzdání diplomových prací a současně Prohlášení autora školního dílana studijní oddělení**
