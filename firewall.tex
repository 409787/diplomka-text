%%
A firewall is a device or, oftentimes in the last few years, a piece of software, that can control network traffic between hosts or networks based on predefined security rule sets. Firewalls, in term of active network devices, can be used for a multitude of purposes. They can even deploy other services, which are not related to flow control. Some examples of such services are dynamic routing, NAT and VPN. These three will be discussed later in this thesis even though they are not the main focus of it. In firewalls, only the traffic that can be understood by the firewall can be handled successfully. A great example of this is the creation of the SSH tunnel. Anything going through the tunnel is encrypted, therefore the firewall does not understand the underlying content. If SSH is part of allowed protocols it is possible for it to contain an underlay protocol that should be dropped.
\par
A firewall policy is a set of rules defined for a specific interface\footnote{The interface can be a physical interface, but also a bond or virtual interface (widely used by VLAN) } and in a specific direction. One interface must have exactly one policy. It is required that a policy has a default rule which is used when a packet does not match any other rules. When using a default drop policy, it is best to use an explicit drop rule right before the end. This can also be used for logging if the firewall's vendor allows logging this way (this can be used by \textit{Cisco ASA} firewalls but not by \textit{iptables}).  Firewall rules are processed sequentially in the single firewall policy.
\par It is sometimes argued whether a firewall brings any level of security. In fact, it is mostly understood as a service for access management, which is actually bound with security. These two terms are often interchanged thus creating confusion between the two and creating the base of this argument.
\section{Typology of firewalls}
  In the last few decades, firewalls have undergone major changes and they no longer represent a homogenous group therefore creating a distinction between the different types needs to be made. Based on their functionality we use the following terms:
  \begin{itemize}
  \item Stateless firewall
  \item Stateful firewall
  \item Application firewalls and proxy gateways
  \item Personal firewall appliances
  \end{itemize}
  \subsection{Stateless firewall}
  A stateless firewall is often called a packet filter. It is the simplest firewall which is tied to routing. It works on the network layer of the ISO/OSI model. However, it can filter traffic based on the L4 ports as well. The functionality of the stateless firewall is as uncomplicated as possible. When a packet arrives to the network interface, the basic router behavior is applied (e.g. correct source according to the interface, checksum...), then the firewall policy is looked up and applied. The most standard behavior is the \textit{first match} which means that the first rule that a packet can match will be applied.\footnote{This can be found in firewalls of different vendors, for example, Cisco or Juniper, but also iptables (a Linux firewall) works this way. A different approach is used by \textit{pf} the OpenBSD firewall which keeps the information till the end of searching and the last one is used. Except for the occasions when this behavior is explicitly overridden.} The rules for the inbound traffic (from the firewall's point of view) are called ingress. Firewalls implement the egress (outgoing/outbound) rules as well. Because the policies may vary on all interfaces, we can have different rule sets for each direction. The network administrator needs to keep this in mind. Not allowing traffic for one of the directions can result in undesired behavior.
  %% The picture \ref{fig:fw1} describes this type of behavior.
%%mozno zrusit obrazok a dalsi odstavec
%  \begin{figure}[h]
%%    \begin{center}
%%      \includegraphics[width=0.85\textwidth]{obr/fw1.pdf}
%%    \end{center}
%%    \caption{Firewall with two directly connected machines}
%%    \label{fig:fw1}
%%  \end{figure}
%%  %%
%%  \par We can see traffic originating from a host with IP address 10.0.0.2 trying to reach a web (HTTP with L4 port 80) server on 192.168.1.2. Each of those is directly connected to the firewall (which is their default gateway). The rules on the side of the web server have to match the rules on the side of the host. The source and destination identifiers need to be changed (the host will try to reach destination 192.168.1.2 and port 80, web server will communicate from this source IP and port). Ingress rules can be seen in the table.\footnote{It should be mentioned that in this example rules with drop at the end are used. So everything that is not explicitly permitted will be denied.}
%  %%
%  \begin{table}
%%  \label{tab:fwrul1}
%%  \caption{Example of firewall rules}
%%  \centering
%%  \begin{tabular}{ | p{\textwidth/4} | p{\textwidth/6} | p{\textwidth/4} | p{\textwidth/6} |  }
%%  \hline
%%  interface 10.0.0.1  ingress & interface 10.0.0.1 egress & interface 192.168.1.1  ingress & interface 192.168.1.1  egress \\ \hline
%%  src ip 10.0.0.2,\newline dst ip 192.168.1.2,\newline src port > 1024,\newline dst port == 80 : permit & & src ip 192.168.1.2,\newline dst ip 10.0.0.2,\newline src port 80,\newline dst port > 1024 : permit  &  \\ \hline
%%  deny & & deny &\\ \hline
%%  \end{tabular}
%  \end{table}
  %%
  \subsection{Stateful firewall}
  The situation is a bit different in the case of stateful firewalls. Most of the aforementioned rules apply with the exception of rules for both directions. The stateful firewall keeps the \textit{state}, in other words it stores some information\footnote{The information stored are IP addresses of both parties, L4 ports, flags, together with sequence numbers, and the state of the session, which is based on TCP flags but is also supported for UDP packets (granting established connection immediately, but even though some UDP connections might have specific requirements).} about the connection currently running. This information is stored in a special table called \textit{session table}. The session table is processed before the firewall rules policy. Therefore it can permit connection without it being explicitly allowed for this direction. It seems more natural to use these types of rules, and they do not require an administrator to synchronize multiple policies.\par
  %%obdobne mozno zrusit tento odstavec, pridat informacie hore kde m ma footnote: Stav je viazany na TCP spojenie... SYNAKCY...
  %% The easiest way to get an insight into the stateful connection is by following an example of a simple TCP connection. Let us consider a similar example to the one used in the previous subsection. A user is trying to reach an HTTP server. He will send a \textit{SYN} packet with a destination port of 80. When this packet reaches the firewall and matches the permissive ingress rule, it immediately creates a \textit{temporary} rule in the session table for the opposite direction. The firewall is now waiting for the \textit{SYN+ACK} packet coming from the HTTP server. It is now not required to hold the rules for the opposite direction in the table because the session table is processed before any other rule. The only other possible use would be if we could assume an initiation of the connection from the HTTP servers port 80. We know that this should never happen and therefore is considered nonstandard behavior and correctly dropped. \par
  %%
  Stateful inspection needs to be enabled by some vendors, for example, the \textit{iptables} require to create a rule with \textit{conntrack} keyword which enables it, others (like \textit{Cisco}) enable stateful inspection globally.
  \subsection{Application firewalls and proxy gateways}
  With growing computational power, a possibility to analyze even more sophisticated parts of networking protocols became achievable. It is best known as a \textit{deep packet inspection} and it is the next generation of firewalls. These firewalls understand some protocol on a deeper level, therefore can check not only the header of the packet but also its body. The application firewalls gained another feature, to some extent similar to the feature of stateful firewalls, that is the connection of multiple bodies. A single command inside a network packet might mean nothing, but combining multiple otherwise meaningless commands may result in malicious behavior. In the same manner, the application firewall may examine the state of the application protocol in contrast with the defined behavior of a standard (often RFC). However, different vendors may implement the standard differently, so, taking this into account, a connection should not be dropped when the behavior does not seems malicious.
  \par
  The implementation of application firewalls, the most potent tool there is, carries some difficulty. Although their bandwidth is limited, they require a considerable amount of computational resources. With growing computational power the speed of the networks increases and as a side effect reduces the convenience of the application firewall solution.
  \par
  Application firewalls are close to the \textit{Intrusion Detection System - IDS}, that is also able to do the deep packet inspection. The main difference between these two is that in IDS there is a possibility to use an anomaly based detection, this means that IDS does not look for malicious behavior by definition, it looks for any suspiciously looking one. \par
  Another feature of the application firewalls or, more precisely, proxy gateways is the possibility to hide a server behind a proxy. The firewall then simulates the service by itself. This means the connection is more secure because there is no direct connection with the server itself. User authentication and encryption can also be a part of a proxy. This, however, lacks the possibility to use bleeding edge protocols and technologies, because they tend to be implemented incompletely, not according to specifications or not be implemented at all.
  %%
  \subsection{Personal firewall appliances}
  Personal firewall is in fact a piece of software protecting the host it runs on. This is what sets it apart because all other previously mentioned network firewalls represent (or at least could represent) the boundaries of different networks they were supposed to protect. Personal firewalls can be found on any modern operating system. \par
  They can have different policies for different users or, in case of laptops and other portable devices, even for different networks they might be connected to.
  %% z
  \section{VPN, NAT}
  \label{sec:vpnnat}
  As previously mentioned, dynamic routing, network address translation and virtual private networks are some of the possible functionalities of firewalls. Unlike dynamic routing, the rest of the three can be, and often is, used on firewalls. This section is focused on some advantages and disadvantages of a firewall providing these services. \par
  Firstly, let's look at network address translation, in this case, the main benefit is also the biggest handicap. When combining the service with the firewall access control, there are real addresses meeting the translated ones.\footnote{In Cisco's terminology we distinguish inside and outside scope as well as local and global addresses. The explanation of those can be found at \url{https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/ipaddr_nat/configuration/15-mt/nat-15-mt-book/iadnat-addr-consv.html\#GUID-8DF8A9A7-4FFF-4AA7-AA93-B3F8FB705C50}.
  %% tu pozriet typograficky ako napisat bodku/footnote podla toho k comu sa viaze
  %% aby bolo jasne ze problem je postupnost vyhodnocovania operacii
  A real address is an address of a computer and a translated one is one affected by NAT. They can differ based on the policy used, and need to be carefully handled.} Ideally, we could be using real addresses without restriction but this is not always the case due to process ordering on the firewall. Moreover, this procedure is not standardized which in the past led to the problem that different versions of firmware (Cisco ASA ver. pre 8.2 and post 8.3) of the same vendor handled the NAT and firewall policy check differently. \cite{cisco82}\cite{cisco83}
  \par Secondly, in the case of virtual private networks, the main benefit is that firewalls often come with the specialized processors for a faster execution of cryptographic tasks. The main disadvantage is that not everything is sped up by the silicone inside the firewall, which leads to some overhead on computational resources. This is true for the NAT as well, administrators should design the networks with this in mind and they can check specifications about which services can be used together and to what extent.
    %% minizaver ze na co je treba brat ohlad?, ze pri vytvarani pravidiel na to treba brat ohlad pre systemoveho administratora
  \par With this in mind, any network administrator has to create rules very carefully, knowing when the rule will apply, and how the packets will look. In other words, they have to ask themselves the question "What will be the content of the header in different points of crossing the firewall?".
  %% dopredny zaznam o , v zavislosti nna type a chovani firewallu, na tuto funkcionalitu sa musi zaoberat aj akykolvek system sahajuci na pravidla. Staci nekonkretne ze je to dolezite a v systeme sa k tomu potom vratit.
  %% konkretnejsi priklad ( nie ake pravidla ale )
  \section{Deploying a firewall into the network}
  %%zmenit company na instittution?"??", ucesat celu tuto kapitolu aby to navazovalo, velkost siete+zachytavanie packetov na zaciatku a na konci...
  When an institution thinks about deployment of a firewall, they need to bear in mind what types of rules will be implemented (respecting the security policy of the company). Due to the fact that each interface can only deploy either an ingress or an egress policy (in case of Cisco, Juniper does not specify direction), company network administrators mostly follow the same practices. These practices may differ throughout companies, for example at Masaryk University, the most widely used rules are the egress rules. This gives administrators the means to troubleshoot the rules in a less burdensome way because each rule is bound with the interface containing resources lying behind it. This approach creates some overhead of computational power required, because every packet, even the one that will be dropped by the rules, will need to be routed there.
  \par The same applies when using multiple firewalls on the track. The traffic that will be dropped in the last firewall, needs to be routed throughout the network, therefore using resources of the network devices on the way. However, the ease of troubleshooting is a major advantage and for this reason, this method is considered a fine solution.
  %%to sa tyka typov firewallov
  When it comes to deciding between different types of firewalls, each company has to take into account what their priorities are. A higher security level, in terms of more advanced features (stateful firewall vs. application firewall), comes at a higher price, therefore a deep packet inspection done in real time is not possible for every connection. There are some possible security mechanisms allowing to disable the source of a malicious behavior \textit{ex post}. In this work, stateful firewalls with a similar setting to those deployed at Masaryk University's Institute of Computer Science will be used.
  \par
  %%
  The main difference between the two mentioned vendors is their approach to the configuration of firewalling rules.\footnote{There are performance differences between the two, e.g. the VPN stability.} The Cisco uses the \textit{access-control list} approach whereas the Juniper uses the zone-based approach. The Cisco uses the zones as well, but the main configuration point where the rules are stored is the access-list. Similarly, Juniper has a sort of access-lists but the configuration is zone-oriented. An example for the Cisco ASA configuration of access-list entry (the access-list is mapped on the ingress or egress of a zone):
  \begin{lstlisting}[
      basicstyle=\small, %or \small or \footnotesize etc.
  ]
  (config)# access-list ACL_IN extended permit ip any any
  \end{lstlisting}
  An example of an entry in Juniper SRX (the security policy is mapped for the traffic between two zones explicitly):
  \begin{lstlisting}[
      basicstyle=\small, %or \small or \footnotesize etc.
      literate={*}{*\allowbreak}1
  ]
  [edit security policies from-zone zone1 to-zone zone2]
  # set policy permit-all match source-address any
  \end{lstlisting}
  \subsection{Troubleshooting}
  %%Nastroje na revizi pravidiel,
  When contemplating network design, there is at least one point that should be as important as the architecture itself and that is troubleshooting. In contrast to design, which happens before the first packet flows through the network, troubleshooting is a longer-distance run. One may argue that troubleshooting occurs after a network is designed and therefore has little to do with the network design itself, but this argument is incorrect. When designing a network we distinguish multiple layers (e.g., access, distribution layer and core), in order to ease the difficulty for future troubleshooting.\cite{ciscoTSHOOT}
  \par
  We might be able to check the connectivity from a source to a remote server using a technique called "follow the path" troubleshooting. By using multiple layers of a network, we are effectively overlapping with another troubleshooting approach as well, called "bottom-up" (starting from the physical layer of the OSI model). We could use a different method and try to split the layers, check the one situated in the middle and depending on our findings move upwards or downwards. This is called the "divide-and-conquer" troubleshooting. The necessity of troubleshooting in networks is undoubtful, and firewalls, like network devices, are and will remain, a part of troubleshooting. In this thesis, some troubleshooting steps will be presented in correspondence with the mentioned principles.
  \par The troubleshooting of firewalling rules may be exhausting. Some tools for a syntax check are available.\footnote{The syntax check is commonly done on the firewall as well, the tools are widely used as a check before automated deployment of rules in a bulk, because a failure of a single rule might not lead into discarding a whole bulk, which then might lead into inconsistencies.} It seems natural that a semantic check of firewalling rules is not straight forward. Two different rule sets might be both syntactically correct but might be affected by some flaws which can lead to improper behavior. It might not be obvious whether the current behavior is the desired one when troubleshooting these rules.
%% syntacticke a semantcike checky do footnoot-u
%%\tiny switch port & \tiny VLAN ID & \tiny VLAN pcp & \tiny  src MAC & \tiny dst MAC & \tiny eth-type & \tiny  src IP & \tiny dst IP & \tiny IP ToS & \tiny IP proto & \tiny L4 sport & \tiny L4 dport \
%%
